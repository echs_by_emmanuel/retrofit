package za.co.retrofit;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface OrderJsonPlaceHolder {

    @GET("Subjects.php")
    Call<List<orders>> getOrders();

    @POST("postjson.php")
    Call<Postid> postorders(@Body JsonObject myjson);
}
