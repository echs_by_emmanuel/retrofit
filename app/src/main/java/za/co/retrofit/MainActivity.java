package za.co.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.POST;

public class MainActivity extends AppCompatActivity {

    private TextView results;
    private String url="http://myeasycloud.co.za/trackingapp/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        results = findViewById(R.id.results);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OrderJsonPlaceHolder orderJsonPlaceHolder = retrofit.create(OrderJsonPlaceHolder.class);

        Call<List<orders>> call = orderJsonPlaceHolder.getOrders();

        call.enqueue(new Callback<List<orders>>() {
            @Override
            public void onResponse(Call<List<orders>> call, Response<List<orders>> response) {
                if(!response.isSuccessful()){
                    results.setText(response.code());
                    return;
                }

                List<orders> morder = response.body();
                for(orders myorder : morder){
                    String content = "";
                    content += "ID: "+myorder.getId()+"\n";
                    content += "Name: "+myorder.getNames()+"\n";
                    content += "Email: "+myorder.getEmail()+"\n";
                    content += "Address: "+myorder.getAddress()+"\n";
                    results.append(content);
                }

            }

            @Override
            public void onFailure(Call<List<orders>> call, Throwable t) {
                results.setText(t.getMessage());
            }
        });
    }
}
