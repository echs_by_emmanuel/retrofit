package za.co.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main2Activity extends AppCompatActivity {
    private String url="http://myeasycloud.co.za/trackingapp/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OrderJsonPlaceHolder orderJsonPlaceHolder = retrofit.create(OrderJsonPlaceHolder.class);

        JsonObject jsonObject=new JsonObject();
        jsonObject.addProperty("id","1");

        Call<Postid> call = orderJsonPlaceHolder.postorders(jsonObject);

        call.enqueue(new Callback<Postid>() {
            @Override
            public void onResponse(Call<Postid> call, Response<Postid> response) {
                if(!response.isSuccessful()){
                    //results.setText(response.code());
                    return;
                }

               Postid morder = response.body();
                Toast.makeText(Main2Activity.this, ""+morder.getStatus(), Toast.LENGTH_SHORT).show();


            }

            @Override
            public void onFailure(Call<Postid> call, Throwable t) {
                //results.setText(t.getMessage());
            }
        });
    }
}
